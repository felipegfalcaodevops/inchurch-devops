data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

data "aws_availability_zones" "available" {}

data "aws_iam_policy_document" "inchurch" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ecs_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}


data "aws_iam_policy_document" "inchurch_data_policy" {
  statement {
    sid = "main"
    actions = [
      "ecs:ListClusters",
      "ecs:DescribeClusters",
      "ecs:ListTasks",
      "ecs:DescribeTasks",
      "ecs:DescribeContainerInstances",
      "ecs:DescribeTaskDefinition",
      "ec2:DescribeInstances"
    ]
    resources = [
      "*",
    ]
  }
}

data "template_file" "inchurch_task_definition_template" {
  template = file("${path.module}/templates/inchurch.task_definition.json.tpl")
  vars = {
    loggroup     = aws_cloudwatch_log_group.inchurch.name
    region       = data.aws_region.current.name
    project_name = local.Project
    repository_url = aws_ecr_repository.this.repository_url
  }
}


data "aws_iam_policy_document" "inchurch_policy" {
  statement {
    sid = "inchurchECSReadAccess"

    effect = "Allow"

    actions = [
      "ecs:ListClusters",
      "ecs:DescribeClusters",
      "ecs:ListTasks",
      "ecs:DescribeTasks",
      "ecs:DescribeContainerInstances",
      "ecs:DescribeTaskDefinition",
      "ec2:DescribeInstances"
    ]

    resources = [
      "*",
    ]
  }
}