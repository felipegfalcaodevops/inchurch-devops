resource "aws_s3_bucket" "source" {
  bucket        = "${local.Name}-${data.aws_caller_identity.current.account_id}"
  acl           = "private"
  force_destroy = true

  tags = {
    Environment = local.Environment
    Management  = local.Management
    Project     = local.Project
    Team        = local.Team
  }
}

resource "aws_iam_role" "codepipeline_role" {
  name = "${local.Name}-role-codepipeline${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"

  assume_role_policy = file("${path.module}/policies/codepipeline_role.json")

  tags = {
    Environment = local.Environment
    Management  = local.Management
    Project     = local.Project
    Team        = local.Team
  }
}

/* policies */
data "template_file" "codepipeline_policy" {
  template = file("${path.module}/policies/codepipeline.json")

  vars = {
    aws_s3_bucket_arn = aws_s3_bucket.source.arn
  }
}

//resource "aws_iam_role_policy" "codepipeline_policy" {
//  name   = "${local.Name}-policy-codepipeline${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
//  role   = aws_iam_role.codepipeline_role.id
//  policy = data.template_file.codepipeline_policy.rendered
//}

resource "aws_iam_policy" "codepipeline_policy" {
  name        = "${local.Name}-policy-codepipeline${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  path        = "/"
  description = ""

  policy = data.template_file.codepipeline_policy.rendered
}

resource "aws_iam_role_policy_attachment" "codepipeline_policy" {
  role       = aws_iam_role.codepipeline_role.name
  policy_arn = aws_iam_policy.codepipeline_policy.arn
}


/*
/* CodeBuild
*/
resource "aws_iam_role" "codebuild_role" {
  name               = "${local.Name}-codebuild-role${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  assume_role_policy = file("${path.module}/policies/codebuild_role.json")

  tags = {
    Environment = local.Environment
    Management  = local.Management
    Project     = local.Project
    Team        = local.Team
  }
  lifecycle {
    ignore_changes = [assume_role_policy]
  }
}

data "template_file" "codebuild_policy" {
  template = file("${path.module}/policies/codebuild_policy.json")

  vars = {
    aws_s3_bucket_arn = aws_s3_bucket.source.arn
  }
}

resource "aws_iam_role_policy" "codebuild_policy" {
  name   = "${local.Name}-policy-codebuild${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  role   = aws_iam_role.codebuild_role.id
  policy = data.template_file.codebuild_policy.rendered
}

resource "aws_codebuild_project" "inchurch_build" {
  name          = "${local.Name}-codebuild${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  build_timeout = "10"
  service_role  = aws_iam_role.codebuild_role.arn

  artifacts {
    //    encryption_disabled    = "false"
    //    name                   = var.name
    //    override_artifact_name = "false"
    //    packaging              = "NONE"
    type = "CODEPIPELINE"
  }

  cache {
    type = "NO_CACHE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:4.0-20.09.14"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"

    environment_variable {
      name  = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }

    environment_variable {
      name  = "PROJECT_NAME"
      value = local.Project
    }

    environment_variable {
      name  = "TEAM"
      value = local.Team
    }

    environment_variable {
      name  = "IMAGE_TAG"
      value = var.image-tag
    }

    environment_variable {
      name  = "IMAGE_REPO_NAME"
      value = "${local.Team}/${local.Project}"
    }

    environment_variable {
      name  = "REGION"
      value = data.aws_region.current.name
    }

  }

  source {
    //    git_clone_depth     = "0"
    //    insecure_ssl        = "false"
    //    report_build_status = "false"
    type = "CODEPIPELINE"
  }

//  vpc_config {
//    security_group_ids = [module.vpc.default_security_group_id]
//    subnets            = module.vpc.public_subnets
//    vpc_id             = module.vpc.vpc_id
//  }

  tags = {
    Environment = local.Environment
    Management  = local.Management
    Project     = local.Project
    Team        = local.Team
  }

  logs_config {
    cloudwatch_logs {
      status = "ENABLED"
    }

    s3_logs {
      encryption_disabled = "false"
      status              = "DISABLED"
    }
  }
}

/* CodePipeline */

resource "aws_codepipeline" "pipeline" {
  name     = "${local.Name}-pipeline${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  role_arn = aws_iam_role.codepipeline_role.arn

  artifact_store {
    location = aws_s3_bucket.source.bucket
    type     = "S3"
  }

  stage {
    name = "Source"

    action {
      category = "Source"

      configuration = {
        BranchName           = var.branch-bitbucket
        ConnectionArn        = aws_codestarconnections_connection.this.id
        FullRepositoryId     = var.repository_id
        OutputArtifactFormat = "CODE_ZIP"
      }
      name             = "Source"
      namespace        = "SourceVariables"
      output_artifacts = ["SourceArtifact"]
      owner            = "AWS"
      provider         = "CodeStarSourceConnection"
      region           = "us-east-1"
      run_order        = "1"
      version          = "1"
    }
  }

  stage {
    name = "Build"

    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      version          = "1"
      input_artifacts  = ["SourceArtifact"]
      output_artifacts = ["imagedefinitions"]

      configuration = {
        ProjectName = "${local.Name}-codebuild${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
      }
    }
  }

  stage {
    name = "Production"

    action {
      name = "Deploy"
      category = "Deploy"
      owner = "AWS"
      provider = "ECS"
      input_artifacts = [
        "imagedefinitions"]
      version = "1"

      configuration = {

        ClusterName = aws_ecs_cluster.inchurch.name
        ServiceName = aws_ecs_service.inchurch.name
        FileName = "imagedefinitions.json"
      }
    }
  }

  tags = {
    Environment = local.Environment
    Management  = local.Management
    Project     = local.Project
    Team        = local.Team
  }
  depends_on = [aws_iam_role_policy_attachment.codepipeline_policy, aws_iam_role.codepipeline_role]
}

