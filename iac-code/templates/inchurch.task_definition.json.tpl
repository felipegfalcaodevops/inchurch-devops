[
  {
    "name": "${project_name}",
    "image": "${repository_url}:latest",
    "entryPoint": [],
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${loggroup}",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "${project_name}"
      }
    },
    "Environment": [],
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      },
      {
        "containerPort": 8080,
        "hostPort": 8080
      }
    ]
  }
]
