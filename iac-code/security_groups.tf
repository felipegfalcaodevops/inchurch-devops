resource "aws_default_security_group" "default" {
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group" "inchurch" {
  name        = "${local.Name}-sg${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  vpc_id      = module.vpc.vpc_id
  description = "Allow SSH, HTTP and HTTPS traffic from ALB"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "inchurch_ecs" {
  name = "${local.Name}-sg-ecs${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"

  vpc_id      = module.vpc.vpc_id
  description = "Allow SSH, HTTP and HTTPS traffic from ALB"

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.inchurch.id]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = [aws_security_group.inchurch.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
