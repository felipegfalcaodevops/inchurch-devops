resource "aws_ecs_cluster" "inchurch" {
  name               = "${local.Name}-cluster${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  capacity_providers = ["FARGATE_SPOT"]
}