resource "aws_lb" "inchurch" {
  name               = "${local.Name}-loadbalance${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.inchurch.id]
  subnets            = module.vpc.public_subnets
}

resource "aws_lb_target_group" "inchurch_api" {
  name = "${local.Name}-tg-8080${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"

  depends_on = [aws_lb.inchurch]

  target_type = "ip"
  port        = 8080
  protocol    = "HTTP"

  vpc_id = module.vpc.vpc_id

  health_check {
    path    = "/health"
    matcher = "200-202,300-302"
  }
}

resource "aws_lb_target_group" "inchurch" {
  name = "${local.Name}-tg-80${local.Name-suffix != "" ? "-${local.Name-suffix}" : ""}"

  depends_on  = [aws_lb.inchurch]
  target_type = "ip"
  port        = 80
  protocol    = "HTTP"

  vpc_id = module.vpc.vpc_id

  health_check {
    path    = "/health"
    matcher = "200-202,300-302"
  }
}

resource "aws_lb_listener" "this" {
  load_balancer_arn = aws_lb.inchurch.arn
  port              = 80

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.inchurch.arn
  }
}

resource "aws_lb_listener" "this_api" {
  load_balancer_arn = aws_lb.inchurch.arn
  port              = 8080

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.inchurch_api.arn
  }
}