resource "aws_ecs_task_definition" "inchurch" {
  family                   = "inchurch"
  container_definitions    = data.template_file.inchurch_task_definition_template.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.ecs_role.arn
  task_role_arn            = aws_iam_role.inchurch.arn
  cpu                      = 256
  memory                   = 512
}