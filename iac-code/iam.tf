resource "aws_iam_role" "inchurch" {
  name               = "inchurch"
  assume_role_policy = data.aws_iam_policy_document.inchurch.json
}

resource "aws_iam_role_policy" "inchurch_policy" {
  name = "inchurch_policy"
  role = aws_iam_role.inchurch.id

  policy = data.aws_iam_policy_document.inchurch_policy.json
}


resource "aws_iam_role_policy" "inchurch_data_policy" {
  name   = "inchurch_data_policy"
  role   = aws_iam_role.inchurch.id
  policy = data.aws_iam_policy_document.inchurch_data_policy.json
}

resource "aws_iam_role" "ecs_role" {
  name               = "ecs_role"
  assume_role_policy = data.aws_iam_policy_document.ecs_role.json
}

resource "aws_iam_role_policy_attachment" "ecs_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs_role.name
}

resource "aws_iam_role_policy_attachment" "ecs_policy_secrets" {
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
  role       = aws_iam_role.ecs_role.name
}