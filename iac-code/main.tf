provider "aws" {
  region = "us-east-1"
  profile = "movecarapps"
}

locals {
  Name        = "inchurch-api"
  Name-suffix = "dev"
  Terraform   = "true"
  Environment = "dev"
  Project     = "inchurch-api"
  Management  = "terraform"
  Team        = "devops"
}

resource "aws_codestarconnections_connection" "this" {
  name = "${local.Project}-codestar"
//  connection_name = "${var.name}-codestar-connection"
  provider_type   = "Bitbucket"
}