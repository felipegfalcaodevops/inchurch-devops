variable "image-tag" {
  default = "latest"
}
variable "branch-bitbucket" {
  default = "development"
}

variable "repository_id" {
  default = "felipegfalcaodevops/inchurch-devops"
}