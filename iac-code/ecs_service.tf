resource "aws_ecs_service" "inchurch" {
  name = "inchurch"

  depends_on = [aws_lb_target_group.inchurch]

  cluster         = aws_ecs_cluster.inchurch.name
  task_definition = aws_ecs_task_definition.inchurch.arn
  desired_count   = 1
  launch_type     = "FARGATE"

//  load_balancer {
//    target_group_arn = aws_lb_target_group.inchurch.arn
//    container_name   = local.Project
//    container_port   = 80
//  }

  load_balancer {
    target_group_arn = aws_lb_target_group.inchurch_api.arn
    container_name   = local.Project
    container_port   = 8080
  }

  network_configuration {
    subnets          = module.vpc.public_subnets
    security_groups  = [aws_security_group.inchurch_ecs.id]
    assign_public_ip = true
  }
}

resource "aws_cloudwatch_log_group" "inchurch" {
  name              = "inchurch-logs"
  retention_in_days = 1
}
