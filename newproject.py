from flask import Flask
from flask import request
from flask import jsonify, make_response

app = Flask(__name__)

@app.route('/health')
def health():
    return "OK"
@app.route("/api/speed")
def host():
    distance = request.args.get('distance', type = int)
    time = request.args.get('time', type = int)

    

    if not distance:           
        response = make_response(jsonify(
                {
                    "error_message": "The parameter 'distance' is required"
                }
            ), 404
        )

    if not time:
        response = make_response(jsonify(
                {
                    "error_message": "The parameter 'time' is required"
                }
            ), 404
        )

    if distance != None and time != None:
        result = distance/time
        text = "{} m/s".format(result)
        
        response = make_response(jsonify(
                    {
                        "speed": text
                    }
                ), 200
        )

    return response



if __name__ == "__main__":
    app.run(host='0.0.0.0')